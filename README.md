# simulatorVek

Esse é o repositório da entrega do teste para vaga de PROGRAMADOR JAVA JÚNIOR 

TECNOLOGIAS UTILIZADAS:
	Spring Boot, JPA, JUnit, H2 Database, JDK 1.8, OpenCSV, Maven, POSTMAN.

PRÉ REQUISITOS PARA INICIAR A APLICAÇÃO:
	JDK 1.8 configurado nas variáveis de ambiente
	(https://www.devmedia.com.br/preparacao-do-ambiente-para-desenvolvimento-em-java/25188) 

Passos para rodar app:
1- baixar - clonar do repositorio gitlab

2- Abrir o terminal de comando e entrar na pasta do projeto;
3- Executar o comando a seguir, no terminal, para iniciar a aplicação:
	java -jar target/simulator-0.0.1-SNAPSHOT.jar

4- Inserir na URL o caminho ("GET") a seguir, para visualizar o resultado da lista de concorrentes inseridos na base de dados:
	(GET)http://localhost:8080/concorrente/

5- Inserir na URL o caminho ("GET") a seguir, para visualizar o resultado da lista de ramos de atividade inseridos na base de dados:
	(GET)http://localhost:8080/ramodeatividade/

6- Inserir na URL o caminho ("POST") a seguir, para visualizar o resultado da simulação da proposta negada:
	(POST) http://localhost:8080/simular/

	***No Body da requisição, inserir o JSON a seguir**
	{
		"id": null,
		"concorrente": {
			"id": 1,
			"nome":"Mercado Pago"
		},
		"ramoDeAtividade": {
			"id": 1,
			"nome":"Loja de roupas",
			"taxaMinimaCredito": 7.0,
			"taxaMinimaDebito": 5.0
		},
		"cliente": {
			"id":null,
			"nome":"Joao",
			"cpfCnpj":"85003611071",
			"email":"joao@gmail.com"
		},
		"taxaDebitoConcorrente":10.0,
		"descontoDebito": 2.0,
		"taxaCreditoConcorrente": 12.0,
		"descontoCredito": 6
	}

7- Inserir na URL o caminho ("POST") a seguir, para persistir a simulação da proposta negada:
	(POST) http://localhost:8080/proposta/

	***No Body da requisição, inserir o JSON a seguir**
	{
		"id": null,
		"concorrente": {
			"id": 1,
			"nome":"Mercado Pago"
		},
		"ramoDeAtividade": {
			"id": 1,
			"nome":"Loja de roupas",
			"taxaMinimaCredito": 7.0,
			"taxaMinimaDebito": 5.0
		},
		"cliente": {
			"id":null,
			"nome":"Joao",
			"cpfCnpj":"85003611071",
			"email":"joao@gmail.com"
		},
		"taxaDebitoConcorrente":10.0,
		"descontoDebito": 2.0,
		"taxaCreditoConcorrente": 12.0,
		"descontoCredito": 6,
		"aceite": false
	}

8- Inserir na URL o caminho ("POST") a seguir, para visualizar o resultado da simulação da proposta permitida:
	(POST) http://localhost:8080/simular/

	***No Body da requisição, inserir o JSON a seguir**
	{
		"id": null,
		"concorrente": {
			"id": 1,
			"nome":"Mercado Pago"
		},
		"ramoDeAtividade": {
			"id": 1,
			"nome":"Loja de roupas",
			"taxaMinimaCredito": 7.0,
			"taxaMinimaDebito": 5.0
		},
		"cliente": {
			"id":null,
			"nome":"Joao",
			"cpfCnpj":"85003611071",
			"email":"joao@gmail.com"
		},
		"taxaDebitoConcorrente":10.0,
		"descontoDebito": 2.0,
		"taxaCreditoConcorrente": 12.0,
		"descontoCredito": 3
	}

9- Inserir na URL o caminho ("POST") a seguir, para persistir a simulação da proposta permitida:
	(POST) http://localhost:8080/proposta/

	***No Body da requisição, inserir o JSON a seguir**
	{
		"id": null,
		"concorrente": {
			"id": 1,
			"nome":"Mercado Pago"
		},
		"ramoDeAtividade": {
			"id": 1,
			"nome":"Loja de roupas",
			"taxaMinimaCredito": 7.0,
			"taxaMinimaDebito": 5.0
		},
		"cliente": {
			"id":null,
			"nome":"Joao",
			"cpfCnpj":"85003611071",
			"email":"joao@gmail.com"
		},
		"taxaDebitoConcorrente":10.0,
		"descontoDebito": 2.0,
		"taxaCreditoConcorrente": 12.0,
		"descontoCredito": 3,
		"aceite": true
	}

10- Verificar no banco de dados em memória as propostas salvas:
	10.1 - No browser, inserir o caminho a seguir na URL para acesso ao banco de dados:
		http://localhost:8080/h2-console/

11- Inserir na URL o caminho ("GET") a seguir, para gerar o arquivo 'propostasValidas.csv':
	(GET) http://localhost:8080/proposta/validas

	11.1- Visualizar o arquivo gerado no diretório raiz do projeto.
