package br.com.simulatortest.simulator;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.simulator.SimulatorApplication;
import br.com.simulator.domain.Cliente;
import br.com.simulator.domain.Concorrente;
import br.com.simulator.domain.RamoDeAtividade;
import br.com.simulator.domain.Simulator;
import br.com.simulator.services.PropostaService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SimulatorApplication.class)
public class SimulatorTest {
	
	@Autowired
	private PropostaService propostaService;
	
	@Test
	public void simulaPropostaValida() {
		Cliente cl;
		Concorrente con;
		RamoDeAtividade rda;		
		
		cl = new Cliente(null, "João da Silva", "122.276.451-21", "joao@email.com" );
		con = new Concorrente(null, "Mercado Pago");
		rda = new RamoDeAtividade(null, "Loja de Roupas", 7.0, 5.0);
		Simulator simulator = new Simulator(null, con, cl, rda, 10.0, 3.5, 12.0, 5.0);
		assertTrue(propostaService.simularProposta(simulator));
	}
	
	@Test
	public void simulaPropostaInvalida() {
		Cliente cl;
		Concorrente con;
		RamoDeAtividade rda;		
		
		cl = new Cliente(null, "João da Silva", "122.276.451-21", "joao@email.com" );
		con = new Concorrente(null, "Mercado Pago");
		rda = new RamoDeAtividade(null, "Loja de Roupas", 7.0, 5.0);
		Simulator simulator = new Simulator(null, con, cl, rda, 10.0, 5.5, 12.0, 7.0);
		assertFalse(propostaService.simularProposta(simulator));
	}
	
}
