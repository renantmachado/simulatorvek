package br.com.simulator.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import br.com.simulator.domain.Proposta;
import br.com.simulator.services.PropostaService;

@RestController
@RequestMapping(value = "/proposta")
public class PropostaResource {
	
	
	@Autowired
	private PropostaService propostaService;
	
	@RequestMapping(value="/", method = RequestMethod.POST)
	public ResponseEntity<Void> insert (@RequestBody Proposta prop){
		prop = propostaService.insert(prop);	
		return null;
	}
	
	

	@RequestMapping(value="/validas", method = RequestMethod.GET)
	public void listarPropostasValidas() {		
		propostaService.validasCSV();
	}

}
