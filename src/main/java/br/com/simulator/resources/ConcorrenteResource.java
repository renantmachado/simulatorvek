package br.com.simulator.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.simulator.domain.Concorrente;
import br.com.simulator.services.ConcorrenteService;

@RestController
@RequestMapping(value = "/concorrente")
public class ConcorrenteResource {
	
	@Autowired
	private ConcorrenteService concorrenteService;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public ResponseEntity<List<Concorrente>> findAll() {		
		List<Concorrente> objs = concorrenteService.findAll();
		return ResponseEntity.ok().body(objs);
	}

}
