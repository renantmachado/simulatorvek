package br.com.simulator.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.simulator.domain.RamoDeAtividade;
import br.com.simulator.services.RamoDeAtividadeService;

@RestController
@RequestMapping(value = "/ramodeatividade")

public class RamoDeAtividadeResource {
		
		@Autowired
		private RamoDeAtividadeService rdaService;
		
		@RequestMapping(value="/", method = RequestMethod.GET)
		public ResponseEntity<List<RamoDeAtividade>> findAll() {		
			List<RamoDeAtividade> objs = rdaService.findAll();
			return ResponseEntity.ok().body(objs);
		}

}
