package br.com.simulator.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.simulator.domain.Simulator;
import br.com.simulator.services.PropostaService;

@RestController
@RequestMapping(value = "/simular")
public class SimuladorResource {

	@Autowired
	private PropostaService propostaService;
	
	@RequestMapping(value="/", method = RequestMethod.POST)
	public ResponseEntity<Boolean> find(@RequestBody Simulator simulator) {		
		boolean obj = propostaService.simularProposta(simulator);
		return ResponseEntity.ok().body(obj);
	}

}
