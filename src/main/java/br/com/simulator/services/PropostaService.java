package br.com.simulator.services;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.opencsv.CSVWriter;

import br.com.simulator.domain.Cliente;
import br.com.simulator.domain.Proposta;
import br.com.simulator.domain.Simulator;
import br.com.simulator.repository.ClienteRepository;
import br.com.simulator.repository.PropostaRepository;

@Service
public class PropostaService {
	@Autowired
	private PropostaRepository repo;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	

	public Proposta insert(Proposta proposta) {
		proposta.setId(null);
		Cliente cliente = new Cliente();
		if(proposta.getCliente().getId() == null) {
			cliente = clienteRepository.save(proposta.getCliente());
		}
		proposta.getCliente().setId(cliente.getId());
		proposta.setCriado(new Date());
		proposta = repo.save(proposta);
		return proposta;
	}

	public boolean simularProposta(Simulator s) {
		return (s.getTaxaDebitoConcorrente() - s.getDescontoDebito()) >= s.getRamoDeAtividade().getTaxaMinimaDebito() 
				&& (s.getTaxaCreditoConcorrente() - s.getDescontoCredito()) >= s.getRamoDeAtividade().getTaxaMinimaCredito(); 
	}
	
	
	public void validasCSV() {
		List<Proposta> propostas = repo.findByAceite(true);
		
		if(!propostas.isEmpty()) {
			String[] cabecalho = {"CRIADO", "CONCORRENTE", "CLIENTE", "RAMO ATIVIDADE", "TAXA MIN. CRED.", "TAXA MIN. DÉB.", 
					"TAXA DÉB. CONCORRENTE", "DESCONTO DÉB.", "TAXA CRÉD. CONC.", "DESC. CRÉD."};
	
		    List<String[]> linhas = new ArrayList<>();
		    for(Proposta p : propostas) {
		    	linhas.add(new String[]{p.getCriado().toString(), p.getConcorrente().getNome(),
		    			p.getCliente().getNome(), p.getRamoDeAtividade().getNome(), p.getRamoDeAtividade().getTaxaMinimaCredito().toString(),
		    			p.getRamoDeAtividade().getTaxaMinimaDebito().toString(), p.getTaxaDebitoConcorrente().toString(),
		    			p.getDescontoDebito().toString(), p.getTaxaCreditoConcorrente().toString(), p.getDescontoCredito().toString()});
		
			    Writer writer;
				try {
					writer = Files.newBufferedWriter(Paths.get("propostasValidas.csv"));
					CSVWriter csvWriter = new CSVWriter(writer);
			
				    csvWriter.writeNext(cabecalho);
				    csvWriter.writeAll(linhas);
				
				    csvWriter.flush();
				    writer.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    }
		} 
		//TODO: Criar ELSE para retornar mensagem informando sem propostas válidas na base
	
	}

}
