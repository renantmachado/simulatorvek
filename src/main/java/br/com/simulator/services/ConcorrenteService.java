package br.com.simulator.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.simulator.domain.Concorrente;
import br.com.simulator.repository.ConcorrenteRepository;

@Service
public class ConcorrenteService {
	@Autowired
	private ConcorrenteRepository concorrenteRepository;
	
	public void save(List<Concorrente> concorrentes) {
		concorrenteRepository.saveAll(concorrentes);
	}

	public List<Concorrente> findAll() {
		return (List<Concorrente>) concorrenteRepository.findAll();
	}
}
