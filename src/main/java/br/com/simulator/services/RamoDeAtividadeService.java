package br.com.simulator.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.simulator.domain.RamoDeAtividade;
import br.com.simulator.repository.RamoDeAtividadeRepository;

@Service
public class RamoDeAtividadeService {

	@Autowired
	private RamoDeAtividadeRepository rdaRepository;
	
	public void save(List<RamoDeAtividade> rdas) {
		rdaRepository.saveAll(rdas);
	}

	public List<RamoDeAtividade> findAll() {
		return (List<RamoDeAtividade>) rdaRepository.findAll();
	}	
}
