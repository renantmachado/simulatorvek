package br.com.simulator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.simulator.domain.RamoDeAtividade;

@Repository	
public interface RamoDeAtividadeRepository extends JpaRepository<RamoDeAtividade, Integer>{

}
