package br.com.simulator.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.simulator.domain.Proposta;



@Repository
public interface PropostaRepository extends JpaRepository<Proposta, Integer>{
	
	@Transactional(readOnly=true)
	public List<Proposta> findByAceite(boolean aceitas);

}
