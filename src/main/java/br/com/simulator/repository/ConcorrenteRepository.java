package br.com.simulator.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.simulator.domain.Concorrente;

@Repository
public interface ConcorrenteRepository extends JpaRepository<Concorrente, Integer> {

}
