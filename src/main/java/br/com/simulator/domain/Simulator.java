package br.com.simulator.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


@Entity
public class Simulator implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne
	@JoinColumn(name="concorrente_id")
	private Concorrente concorrente;
	
	
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name="ramo_atividade_id")
	private RamoDeAtividade ramoDeAtividade;
	
	private Double taxaDebitoConcorrente;
	private Double descontoDebito;
	private Double taxaCreditoConcorrente;
	private Double descontoCredito;
	
	public Simulator() {
		
	}
	
	public Simulator(Integer id, Concorrente concorrente, Cliente cliente, RamoDeAtividade ramodeAtividade,
			Double taxaDebitoConcorrente, Double descontoDebito, Double taxaCreditoConcorrente,
			Double descontoCredito) {
		super();
		this.id = id;
		this.concorrente = concorrente;
		this.cliente = cliente;
		this.ramoDeAtividade = ramodeAtividade;
		this.taxaDebitoConcorrente = taxaDebitoConcorrente;
		this.descontoDebito = descontoDebito;
		this.taxaCreditoConcorrente = taxaCreditoConcorrente;
		this.descontoCredito = descontoCredito;
	}

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Concorrente getConcorrente() {
		return concorrente;
	}


	public void setConcorrente(Concorrente concorrente) {
		this.concorrente = concorrente;
	}


	public Cliente getCliente() {
		return cliente;
	}


	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}


	public Double getTaxaDebitoConcorrente() {
		return taxaDebitoConcorrente;
	}


	public void setTaxaDebitoConcorrente(Double taxaDebitoConcorrente) {
		this.taxaDebitoConcorrente = taxaDebitoConcorrente;
	}


	public Double getDescontoDebito() {
		return descontoDebito;
	}


	public void setDescontoDebito(Double descontoDebito) {
		this.descontoDebito = descontoDebito;
	}


	public Double getTaxaCreditoConcorrente() {
		return taxaCreditoConcorrente;
	}


	public void setTaxaCreditoConcorrente(Double taxaCreditoConcorrente) {
		this.taxaCreditoConcorrente = taxaCreditoConcorrente;
	}


	public Double getDescontoCredito() {
		return descontoCredito;
	}


	public void setDescontoCredito(Double descontoCredito) {
		this.descontoCredito = descontoCredito;
	}
	public RamoDeAtividade getRamoDeAtividade() {
		return ramoDeAtividade;
	}
	
	public void setRamoDeAtividade(RamoDeAtividade ramoDeAtividade) {
		this.ramoDeAtividade = ramoDeAtividade;
	}
}
