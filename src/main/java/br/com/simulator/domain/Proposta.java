package br.com.simulator.domain;

import java.util.Date;

import javax.persistence.Entity;

@Entity
public class Proposta extends Simulator {
	
	private static final long serialVersionUID = 1L;
	Boolean aceite;
	Date criado;
	public Proposta() {
		
	}
	public Proposta(Boolean aceite, Date criado) {
		super();
		this.aceite = aceite;
		this.criado = criado;
	}
	public Boolean getAceite() {
		return aceite;
	}
	public void setAceite(Boolean aceite) {
		this.aceite = aceite;
	}
	public Date getCriado() {
		return criado;
	}
	public void setCriado(Date criado) {
		this.criado = criado;
	}
	
	
}
