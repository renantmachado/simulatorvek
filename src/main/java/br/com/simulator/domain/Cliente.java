package br.com.simulator.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Cliente implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String nome;
	private String cpfCnpj;
	private String email;
	
	//@OneToMany(mappedBy="cliente")	
	//private List<Simulator> simulators = new ArrayList<>();
	
		
	public Cliente(){
		
	}
	public Cliente(Integer id, String nome, String cpfCnpj, String email) {
		super();
		this.id = id;
		this.nome = nome;
		this.cpfCnpj = cpfCnpj;
		this.email = email;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	//public List<Simulator> getSimulators() {
	//	return simulators;
	//}
	//public void setSimulators(List<Simulator> simulators) {
	//	this.simulators = simulators;
	//}
	

}
