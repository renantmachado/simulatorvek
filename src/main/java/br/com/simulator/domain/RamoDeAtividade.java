package br.com.simulator.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class RamoDeAtividade implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String nome;
	private Double taxaMinimaCredito;
	private Double taxaMinimaDebito;
	
	//@OneToMany(mappedBy="ramoDeAtividade")	
	//private List<Simulator> simulators ;
	
	public RamoDeAtividade() {
		
	}
	public RamoDeAtividade(Integer id, String nome, Double taxaMinimaCredito, Double taxaMinimaDebito) {
		super();
		this.id = id;
		this.nome = nome;
		this.taxaMinimaCredito = taxaMinimaCredito;
		this.taxaMinimaDebito= taxaMinimaDebito;
	}
	public Double getTaxaMinimaCredito() {
		return taxaMinimaCredito;
	}
	public void setTaxaMinimaCredito(Double taxaMinimaCredito) {
		this.taxaMinimaCredito = taxaMinimaCredito;
	}
	public Double getTaxaMinimaDebito() {
		return taxaMinimaDebito;
	}
	public void setTaxaMinimaDebito(Double taxaMinimaDebito) {
		this.taxaMinimaDebito = taxaMinimaDebito;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	/*public List<Simulator> getSimulators() {
		return simulators;
	}
	public void setSimulators(List<Simulator> simulators) {
		this.simulators = simulators;
	}*/
	
	
	

}
