package br.com.simulator;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.simulator.domain.Concorrente;
import br.com.simulator.domain.RamoDeAtividade;
import br.com.simulator.repository.PropostaRepository;
import br.com.simulator.services.ConcorrenteService;
import br.com.simulator.services.RamoDeAtividadeService;

@SpringBootApplication
public class SimulatorApplication implements CommandLineRunner {

	@Autowired
	PropostaRepository repo;	
	
	@Autowired
	private ConcorrenteService concorrenteService;

	@Autowired
	private RamoDeAtividadeService rdaService;
	
	public static void main(String[] args) {
		SpringApplication.run(SimulatorApplication.class, args);
	}
	
	@Override
	public void run(String... args) throws Exception {
		

		Concorrente concorrenteA = new Concorrente(null, "MercadoPago");
		Concorrente concorrenteB = new Concorrente(null, "MercadoLivre");
		Concorrente concorrenteC = new Concorrente(null, "PagueSeguro");
		
		concorrenteService.save(Arrays.asList(concorrenteA, concorrenteB, concorrenteC));
		
		RamoDeAtividade rda = new RamoDeAtividade(null, "Loja de Roupas", 7.0, 5.0);
		RamoDeAtividade rda1 = new RamoDeAtividade(null, "Loja de Calçados", 8.0, 6.0);
		RamoDeAtividade rda2 = new RamoDeAtividade(null, "Agropecuária", 7.5, 5.2);
		
		rdaService.save(Arrays.asList(rda , rda1, rda2));
		
	}
	
	/*@Override
	public void run(String... args) throws Exception {
	
		Proposta pro = new Proposta();
		Cliente cl;
		Concorrente con;
		RamoDeAtividade rda;
		
		
		cl = new Cliente(null, "João da Silva", "122.276.451-21", "joao@email.com" );
		con = new Concorrente(null, "Mercado Pago");
		rda = new RamoDeAtividade(null, "Loja de Roupas", 7.0, 5.0);
		pro.setConcorrente(con);
		pro.setCliente(cl);
		pro.setRamodeAtividade(rda);
		pro.setTaxaDebitoConcorrente(10.0);
		pro.setDescontoDebito(3.5);
		pro.setTaxaCreditoConcorrente(12.0);
		pro.setDescontoCredito(5.0);
		pro.setAceite(true);
		Date dt = new Date();
		pro.setCriado(dt);
		repo.save(pro);
	}*/

}

